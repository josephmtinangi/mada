@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Mada Mpya</div>

                <div class="panel-body">
                   
                   <form action="{{ route('topics.store') }}" method="POST">
                   	{{ csrf_field() }}


                   	<div class="form-group">
                   		<label for="title">Kichwa</label>
                   		<input type="text" name="title" id="title" class="form-control" required>
                   	</div>

                   	<div class="form-group">
                   		<label for="body">Mada</label>
                   		<textarea name="body" id="topic-ckeditor" class="form-control"></textarea>
                   	</div>

                   	<button type="submit" class="btn btn-primary">Post</button>
                   	
                   </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'topic-ckeditor' );
</script>

@endpush
